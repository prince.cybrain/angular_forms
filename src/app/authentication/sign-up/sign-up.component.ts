import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit{
  signupForm!: FormGroup;
   visble = false;

  constructor(private fb: FormBuilder,private router: Router) { }
  ngOnInit(): void {
    this.signupForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      address: ['', Validators.required],
    });
    this.click()
  }
  click(){
    this.visble = false

  }
  onSubmit(): void {
    if (this.signupForm.valid) {
      console.log(this.signupForm.value);
      this.router.navigate(['/dashboard/analytics'])
      this.visble = true;
    } else {
      this.signupForm.markAllAsTouched();
    }
  }
  onCancel() {
    window.location.reload();
  }
  }
